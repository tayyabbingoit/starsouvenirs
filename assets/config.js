// cpanel - site_templates/countdown_tech/assets/config.js.tt Copyright(c) 2016 cPanel, Inc.
//                                                          All rights Reserved.
// copyright@cpanel.net                                        http://cpanel.net
// This code is subject to the cPanel license. Unauthorized copying is prohibited

window.cpanel = {
    data: {
        email: "info@bingoit.co",
        logo: "Starsouvenirs",
        social: [
            
            
            
            
            
            
        ]
    },
    style: {
        primary: "",
    },
    slides: [
        {
            type: 'countdown',
            backgroundImage: "",
            backgroundColor: "",
            color: "",
            buttonText: "",
            buttonLink: "",
            endTime: '2018-11-15T09:03:00.000Z'
        }
    ]
};
